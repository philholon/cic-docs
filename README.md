## Community Inclusion Currencies (CICs) Documentation
Community Inclusion Currency (CIC) technology seeks to give organizations and communities the ability to issue, use and manage CICs in order to build resilient and connected economies.
 + [White Paper](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/CIC-White-Paper.pdf)
 + [Training Materials](https://docs.google.com/document/d/1CI-Xz9R3FgZ70uWgq81U5uwfaYrm9f6Ji4f7dL2-V90/edit?usp=sharing) 
 + [Frequently Asked Questions](https://docs.google.com/document/d/1qtlOEL4pqW1vOL893BaXH9OqRSAO3k0q2eWbVIkxEvU/edit?usp=sharing) 
 + [Blog](https://grassecon.org/blog)
 + [MOOC](https://grassecon.org/mooc) - Very old (paper based systems)
 + [Village Market Simulator series](https://www.youtube.com/playlist?list=PLPUExzwZAUpbEInJy_8Wj_c_mDsw7-qXe)
 + [Transaction Data CSVs](https://www.grassrootseconomics.org/research): Transaction Datasets and Research
 + [Live Data Dashboard](https://dashboard.sarafu.network)
 
 ## Open source code
 + [Managment Platform](https://gitlab.com/grassrootseconomics/cic-platform)
 + [Dashboard](https://github.com/GrassrootsEconomics/Accdash) [Dashboard API](https://github.com/GrassrootsEconomics/Accap)
 + [Blockchain Smart Contract](https://github.com/bancorprotocol/contracts) 

## CIC full system vision ##
 * Blockchain
   * Currently: We are on a public blockchain (Layer 2 side chain) called [xDAI](https://www.xdaichain.com/) 
   * Goal: To ensure decentralization, censor proofing and fair gas prices we plan to setup a validation node on this chain and look into possible Humanitarian Blockchains.
 * Blockchain Contracts
   * Bridge - in order to move between the xDAI side chain (Layer 2) and the Ethereum main net we need a bridge (hard spoon).
     * Currently: We connect to the Ethereum Main net using the xDAI to DAI bridge.
     * Goal: To have options to bridge to a variety of potential reserves
   * Bonding Curves - Enable us to add collateral (aka Reserves to CICs) as well as connect CICs to each other in a safe way.
     * Currently: Bancor Protocol v1 - [Blockchain Smart Contract](https://github.com/bancorprotocol/contracts)
     * Goal: Researching Bancor Protocol v2 and other options
   * Contract Deployment (Currency Creation service)
     * Currently: We currently deploy the Bancor Contract suite manually via Command line
     * Goal: Both a USSD based and a web based interface for organizations and communities to create their own tokens
* CIC Token Creation system
     * Currently: Manual contract deployment (command line)
     * Registered organizations can submit an application to be an issuer, they must have:
        * an audited and legally binding backing commitment signed by local government to back the total amount of a CIC .
        * A collateral fund of at 25% of the tokens they want to create. 
          * This collateral can be pulled out of any CIC (such as those provided by an Aid CIC) or purchased 
        * These new CIC tokens are created and the converter contract is given the sole ability to create and destroy them based on a bonding curve equation THe converter also holds the collateral (reserve or pool) of xDAI 
* Cloud server - is where we host all of our systems, Platform, Dashboard, future blockchain node.  
   * Currently: We are using AWS for instances and data storgage along with postgress databases - Transactions are done here first then synced with blockchain
   * Goal: Moving toward a webased wallet and cloud storage
 * Dashboard
   * Currently: [Our dashboard](https://dashboard.sarafu.network) uses a D3 javascript we also have an internal dashboard for CIC platform administrators. 
     * We are pulling data from an internal postgres database on the CIC Mgmt Platform as well as blockchain transaction data from BlockScout
   * Goal: To have system related information available to field officers and users on Android phones and to pull data from our own blockchain node.
     * To allow donors to give directly and measure the impact of their giving
     * To visualize trade and the health of economies and communities
 * Simulations 
   * Currently: We started with a home grown Village Market Simulator and are currently are developing and using a cadCAD based [modeling](https://gitlab.com/grassrootseconomics/cic-modeling) simulation built by BlockScience
   * Goal: To build on this simulation to validate and stress test economic models and to provide predictive analysis based on data
 * Marketplace
   * Currently: Mostly word of mouth but Grassroots Economics provides limited signage to users and marketing of products in the network is done by word of mouth, SMS messaging and a phone support team.
   * Goal: A web based interface for organizations and communities to share projects, event offers and wants using CICs
 * CIC Management Platform 
     * Currently: System admins (GE staff) are able to assist users to reset pins, reverse transactions, create new users. The system is build on React and Flask in Python and uses postgres for data stroage.
     * Goal: Stabilization of the platform and synchronization to blockchain, and eventual migration to a webBased wallet system with cloud storage with minimal need.
 * Wallet:
     * Currently: Users connect via USSD through AfricasTalking to connect with their wallet on the CIC Mgmt Platform, private keys are held by Grassroots Economics for these phones
     * Goal: 
       * WebApp 
       * Non custodial
       * Social account recovery
       * Cloud data storage (Swarm/IPFS) - opt in data, with an option sharing and for 3rd party validation
       * Market Place linkages
       * Auto-conversion – so you only have one (CIC) token (your community token) - all other tokens auto convert
       * CIC aware 
         * you can change your home token to another
         * You can see the stats of any CIC you want to trade with or change to
         * You can see the effect of cashing in and cashing out on the CIC
       * Payment Rails
         * Ability to add National currency (Mpesa) – in order to create/buy more CIC
         * Ability to liquidate a CIC for National Currency (Mpesa)
 * Fiat On-ramp / off-ramp - In order to enable users and donors to interact with CICs we need a way to move fiat on and off the blockchain
   * Currently: the float account is in no way linked to blockchain and handled manually (bank transfers)
     * We currently move Fiat (USD/EUR) on an off the blockchains we use via exchanges such as BitCoinSwiss and local cryptos connected to Banks as well as eMoney
     * You can send Mpesa (eMoney) to our lipa na Mpesa account – GE sees that and sends back tokens
     * You can also send tokens to GE and GE sends back Mpesa from the float account (this currently very restricted)
   * Goal: To make credit card another other payment on and offramps available and streamline our fiat to token processes ... blockchain integration
   * Goal: As there is imbalance in the float account – Tokens can be burnt (xDAI extracted – bridged to DAI  (eg see xDAI.io) → converted to Eth and sold for KSH (BitcoinSuisse  ~2% fee)
     * If there is too much KSH – we would send it to (BitcoinSuisse ~2% fee) to convert to Eth then make DAI with it and bridge to xDAI (eg see xDAI.io)
     * The float account balancing would happen on a monthly cycle (ideally) – initially manual and eventually automated
     * Exchange Rates will be variable based on the CIC → xDAI rate (depends on amount of collateral) Spot price: P  = 4R/S (R= reserve S = Supply) but they will also take into account FX fees.
 * Blockchain interaction (once connected to xDAI reserves)
   * Anyone who can obtain Eth or DAI can create xDAI and send it directly to any CIC converter blockchain contract. They will mint an amount of that CIC based on the bonding curve equation. 
   * Anyone can also send a CIC to the same contract to liquidate it and pull out the xDAI and convert to DAI or ETH. Note that there are markets to cash out ETH in Kenya such as LocalCryptos as well but at high costs (~10%).
   * Note that you can also buy Eth in Kenya with Mpesa on local cryptos and could convert it to xDAI then add it to a CIC reserve.
 * ID System - 
   * Currently: Each user has a sim card and the Telecom provides KYC services - users are asked a pin number to access the account on USSD 
   * Goal: Creating a more robust ID system for users with and without sim cards. Possibly adding brightID social ID  - like technologies with confidence scoring or 3rd party biometrics
     * Non custodial data ownership on cloud where users can specify what data tehy want to make avalible
 * Rewards and Incentives - [see specifications](./spec/001_platform_incentives.md)
     * Currently: These incentive systems are a mixture of manual and CIC Mgmt Platform base, such as initial user air drop, referral bonuses and holding fees.
     * Currently: We are using a CIC by the name of Sarafu that is backed by Grassroots Economics' (donor funds). Users get a Sarafu for free and groups will be able to create their own CICs by pulling the xDAI out of Sarafu
     * Goal: Any org developing a CIC can easily establish various rules and policies
 * Training Materials:
   * Currently: We are developing curriculum, games and materials for training communities and CIC issuers 
    
 