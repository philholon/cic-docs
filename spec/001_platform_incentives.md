# Current Platform Incentives spec

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <willruddick@gmail.com> (grassecon.org)
* Date: 2020.04.22
* Version: 1
* Status: Pre-draft

## Rationale
The current system seeks to maximize circulation in order to fill market gaps and help CIC users to support eachother.

## Currently - bonding CIC issuance to a reserve with no market value.
Grassroots Economics created a pool of 16 Million Sarafu tokens (which they value 1:1 with Kenyan Shillings ala buy backs - see below). 
Of which 60% will have been distributed to users as below by mid May 2020. 
The following are all rules are platform based (not blockchain contract based). Note that these rules are for the Sarafu CIC only and are not meant to be the rules for other CIC creators.
1. Outgoing Sarafu
   1. **airdrop-selfservice**: New users get 50 Sarafu (a CIC) (Shown as Disbursement in database)
   1. **airdrop-full-profile**: New users that have traded outward and fill out their profiles (name, business, location, gender) get additional 350 Sarafu (a CIC)  (Shown as Disbursement in database)
   1. **referral**: Users that are the 1st to send a new user at least 20 tokens get 100 Sarafu - after that new user has begun to trade with another user (not the referrer) (Shown as Disbursement in database)
   1. **chama bonus**: Savings Groups (aka Chamas are groups of users that save and give loans to eachother) get 10,000 Sarafu divided between their members after 2 months of trading (done by office manually).(Shown as Disbursement in database)
   1. **basic income / usage bonus**: Users get a daily or weekly Sarafu bonues depending on how they trade. They are ranked by the number of other people they trade above 20 Sarafu with and awarded based on their percentage of such overall trade. (Possibly moving to k-cycle centrality).(Shown as Disbursement in database)
   1. **donation bonues** Anyone with Mpesa or Bonga points (National Currency eMoney) can send them to Grassroots Economics to receive additional Sarafu (Shown as Disbursement in database) Donors can give Grassroots Economics funds to support the Sarafu buy-back and operations
1. Incomming Sarafu 
   1. **holding Fee**: If an account if dormant (no trades in or out) for 30 days 100 Sarafu are deducted and added back to the pool. (Shown as Reclaimation in database)
   1. **chama redemption**: Savings Groups can redeem 50% of their Sarafu balance for Kenyan Shillings up to a maximum of 30k Sarafu each month but must have spent (and/or given loans) of at least as much as they want to cash out. (Shown as Agent_out in database) Cashing out is done using donor funds by Grassroots Economics Foundation and Sarafu is valued 1:1 with Kenyan shilligns using eMoney (Mpesa).
   1. **minting/creating**: Currently the Sarafu supply is bonded to a virtual reserve token with no market value so there are no restrictions on the supply of Sarafu

## After - bonding to a reserve is market value.
After the migration to xDAI as reserve - see [migration spec](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/002_xdai_migration.md).
anyone (with access to blockchain) in the world can contribute reserve in the form of xDAI. xDAI is a stable token to the US dollar and can be purchased with USD. 
With a reserve in xDAI each Sarafu token will now have spot price or excahnge price to xDAI given by P = R/(S*TRR)
Where R = the amount of xDAI in reserve, S= the total supply of Sarafu, and TRR = Target Reserve Ratio = that ratio of R/S such that the echange price is 1:1.
1. **Off chain incentives** - Grassroots Economics will continue with all the above incentives for Sarafu and to buy the vouchers off on a regular basis 1:1 from Savings groups.
1. **Adding Reserve off bonding curve - depth bump** - Donors can choose to increase the reserve amount and mint tokens without chaning the excahnge price - see [Depth Bump SPec](https://gitlab.com/grassrootseconomics/cic-docs/-/blob/master/spec/003_depth_bump.md)
1. **Adding Reserve via bonding curve** - Anyone with access to the blockchain (via a webApp or MetaMask) can add xDAI and mint more Sarafu (+) increase the value of all Sarafu
  1. As the reserve of Sarafu is depleted and the exchange price drops Grassroots Economics as well as other donors will add more reserve and mint more tokens to distribute.
1. **Liquidating CICs** - Anyone with access to the blockchain (via a webApp or MetaMask) can add send Sarafu to the converter contract to destroy it and withdraw reserve (-) decreasing the value of all Sarafu.
  1. **Usage of on-chain reserves** - Grassroots Economics will liquidate Sarafu it collects to pull out excess reserve and convert it (xDAI) to Kenyan shillings to continue with Savings Group buy backs.
1. **Conversion fee** - A % fee on conversion between reserve and supply in both directions (minting or liquidating tokens) is done on blockchain and added to the reserve.


## Implementation

### Workflow

### Variables

### Interface


## Testing
<!--
Please describe what test vectors that are required for this implementation
-->

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
