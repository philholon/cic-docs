# Depth Bump - Add new tokens to reserve and supply without effecting price

Author: Will Ruddick
Date: 2020.03.16
Version: 0.0.3

## Rationale

The reserve on the our bonding curve contracts will never grow far beyond the Target Reserve Ratio (TRR) point as holders will sell off the CICs and remove the reserves beyond it.
Giving people the option to multiply the reserve and supply tokens with the same multiple (without a price change) would be a way to increase the overall liquidity. 
But because that would give an advantage to liquidity providers when the CIC price is above 1:1 we need to ensure that excess tokens are given to the community - in the form of a Basic Income pool.

### Note on depth_shrink

While a depth_bump gives people the ability to grow the liquidity ppol while taking a loss and as a result incresing the Basic Income 
- a depth shrink (spec comming) can have the same effect by reducing the liquidity depth.
- The result of a depth bump and depth shrink means that a market can determine the appropriate liquidity depth.

## Before

We have no ability to increase liquidity depth (reserve and supply outside of the bonding curve) in the case where a system wants to grow without effecting the price. 
Example if a donor decides to add 40,000 in reserve to a CIC (which is already at the designated TRR) 
the price of the token may shoot up and CIC sell offs will cause the reserve to fall back again 
  - i.e. the Donor didn't really increase the over depth of the liquidity pool they just added a temorary price bump.

## After

We would need to be able to mint tokens and the reserve outside of the bonding curve in a controled way.
The idea would be to keep the CIC price 1:1 with reserve and increase both reserve and supply equally (with the same multiple) (off the curve)
To avoid gaming: Excess tokens created in this process (beyond those minted along the bonding curve) should be given to a specific address designated in the Converter constructor.
A donor in this case can choose to always get back as many CICs as they add to reserve - with no speculative advantage.

## Implementation

### Workflow

* A public depthBump Funcation is added to the converter contract which takes in reserve R1 
* Calculate the multiple to teh reserve (R1/R0) and mint an amount of tokens T1 = T0(R1/R0)  - Where R0 and T0 are the inital amount of reserve and supply
* Calculate the amount of tokens (Tc) that would have been created on the bonding curve due to R1.
* If the tokens T1 > Tc then Te = (T1-Tc) and Te goes to a desginated wallet (Community Pool) and Tc goes to whomever added the R1 (thus not giving the liquidity provider a advantage)
* If the tokens T1 <= Tc then T1 goes to whomever added the R1 (in this case the liquidity provider takes a loss)

### Variables

* Amount of Reserve to add R1 8,000,000
* A community Pool account (for excess) in the constructor of the converter contract
* Note taht this pool may be used for other system functions like rewards and holding fees

### Interface (CLI)

cmd: depthbump <-r reserve added> 

## Testing

```
cmd: depthbump -r R1 (R1 in units of the Reserve)

You are creating R1 new tokens? (yes/no/quit)

....

the new supply of CIC should be: The old supply * (R1/R0)
the new reserve should be: The old reserve + R1
the new excahnge price should be the same as the old price
P0 = R0/(S0*TRR) = P1 = R1/(S1*TRR)

```


## Changelog

* 0.0.3: Simplification of original concept with addition of a community pool
* 0.0.2: Clean up formatting, subsection implementation
* 0.0.1: Created initial stub
