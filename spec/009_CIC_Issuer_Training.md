# Community Inclusion Currencies (CICs) Training Course

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Will Ruddick <will@grassecon.org> (grassecon.org)
* Last Edit Date: 2020.04.30
* Version: 1
* Status: Pre-draft

## **Rationale**

Groups creating Community Inclusion Currencies need to understand the history, benefits, obligations and risks - as do organizations using and training groups.


## **Prerequisites**


### **Students**

Local students, businesses, group members, CIC designers, design challenge participants, Humanitarian aid Ideally the students have an understanding of local markets and ideally also sell goods or services themselves that are desirable to the other participants


### **Focus**

Community groups, self help groups, SILCs, VSLAs, Chamas, Savings and Loan groups Group members should consist of people with local business and jobs that can trade amongst each other Trainers could be Red Cross employees or volunteers with adequate background.


## **Goals**

Key goals include:



1. The group trained has the understanding, capacity and ability to:
    1. create a clear statement of:
        1. intent to issue and redeem a CIC in stated goods and services.
        2. Purpose and vision for the CIC - what it will be used for.
    2. Move forward in CIC development
2. The group further understands
    3. the commitments, risks and obligations associated with issuing a CIC
    4. how to bring on more community support and acceptance of their CIC
    5. how to use as well as mint and destroy CICs
    6. the community vision
    7. how to measure success, failure and impact related to CIC usage
    8. what backs a CIC locally as well as hard collateral
    9. their relationship to the local economy and other community members
    10. the technology for creating and using CICs
    11. their role as a stakeholder in a economy and a CIC created


## **Implementation**



1. Duration: This is a suggested 4 part course that could be taken in 4 consecutive sessions - spanning a day, days or a month.
2. Facilitation: Minimum 1 facilitator with experience training other groups and being part of a CIC
3. Materials: Students should have
    1. a notebook and pencil.
    2. 2 different colored denominated Tokens - A stack of small pices of paper (business card sized) will do
    3. A container for the the tokens above that will represent the reserve (collateral)
    4. *Mobile phone (if USSD available any mobile phone - otherwise a smart phone with internet enabled)
    5. *A projector or ability to play a short video (groups around an andriod phone could work)
*   Optional but recommended.


## **Outline**



1. Community Inclusion Currencies
    1. Introduction
    2. History (*Video)
    3. Examples (paper and mobile phones)
    4. CIC Game*
    5. Basic CIC Usage
        1. Buying and selling
        2. Your pin and profile information
        3. Marketplace
    6. Your Economy
        4. Circular trade
        5. Under utilized potential
        6. Stronger local markets
2. Creating a CIC
    7. Who should create a CIC?
    8. Resource Mapping
    9. Issuer Commitments
    10. How many CICs to create?
    11. Issuer Backing and Redemption
    12. Reserves - CIC Collateral
        7. Cashing Out and Cashing In
        8. How does it work?
        9. What happens then?
        10. Back to Social Backing
3. Introducing CICs to the rest of community
    13. Establishing a solid foundation
    14. Good Communication
    15. Spreading CICs
        11. Airdrops
        12. Rewards
        13. Income Salaries
        14. Spending
        15. Sharing
    16. Connecting to other CICs
        16. How does it work?
        17. Leaving or joining a CIC
        18. Auto conversion
4. Getting Started
    17. Risks
        19. Obligations
        20. Dispute Mitigation
    18. Documenting Commitment
    19. Trial Period
    20. Bootstrapping with Sarafu 
    21. Seeding Reserves


## Testing
<!--
Please describe what test vectors that are required for this implementation
-->

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
