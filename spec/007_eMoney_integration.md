## Incoming Sarafu ##
When users exchange CIC <> National Currency we have to manually send them Mpesa or CIC.
We would like to automate this.

## Solution ##
Configure - using our own system AfricasTalking API to automatically send CIC / Mpesa based on rules:
- CIC - > Mpesa 
   - CICs are converted to reserve on-chain and the xDAI is held in a GE account (xDAI Float)
   - We send back an equivalent amount of Mpesa to the user from out MPESA float account
- Mpesa -> CIC
   - Mpesa are added to our float Mpesa account.
   - xDAI from our xDAI float account is added to the reserve of the CIC (Home CIC of the user) and new CICs are minted and sent to the user


### Variables ###
 - blockchain_CIC_Supply = the total supply of the CIC on the blockchain
 - blockchain_CIC_reserve = the total reserve (xDAI) of the CIC on the blockchain
 - trr = the connector weight on blockchain converter between supply and reserve
 - FLoat xDAI: THe amount of xDAI we have in our master wallet
 - FLoat Mpesa: THe amount of Mpesa supply we have in our PayBill account
 - CIC_excahnge_amount = the amount of CIC they user has sent to exhange
 - Mpesa_excahnge_amount = the amount of MPesa the user has sent to exhange
 - Rate: USD -> KSH
 - Rate: KSH -> USD
 - Fees: Sarafu to Mpesa FeeCC2M = 2%
 - Fees: Sarafu to Mpesa FeeM2CC = 2%
 - The account to which CIC are sent should be a Agent account
 - The cash-out rules for a chama (minimum and time limit)
 - Alert balance levels
 
### Incoming CIC Rules - MPESA out ###
 - *white list* The chama (group accounts only but eventually anyone) can cash out - note the chama limits
 - *amount* See KYC_limit max, supply_max
 - *mpesa given* The amount of Mpesa given should be calculated as follows:
 - CIC_excahnge_amount = the amount of CIC the user wants to excahnge

The Reserve (xDAI) to be pulled out (reported):

- reserveOut_xDAI_reported = the amount of xDAI that would be pulled from the reserve on-chain (by getting a quote on-chain)
- reserveOut_Mpesa = reserveOut_xDAI_reported **USD => KSH** rate
- reserveOut_Mpesa_reported = reserveOut_xDAI_reported **USD => KSH** rate

After the user confirms:

The Reserve (xDAI) actually pulled out 
- reserveOut_xDAI = the amount of xDAI actually pulled from the reserve on-chain
-  **Mpesa_out** = reserveOut_Mpesa_reported (this is actually given to the user) - **and GE takes on the Mpesa risk**
  -  reserveOut_Mpesa = reserveOut_xDAI **USD => KSH** rate (this is from our MPESA float account)
  -  Mpesa_risk = Mpesa_out - reserveOut_Mpesa - this should me monitored and recorded for each transactions


### Outgoing Sarafu Rules - MPESA In ###
 - *white list* Only users sending via paybill receive Sarafu. Non users receive a invitation to join Sarafu sms (alert to admins there was a donation).
 - *amount* The max sarafu out is 20,000 Sarafu per user, KYC_limit max, supply_max
 - *time* No limit
 - *Mpesa_excahnge_amount* The amount of CIC given should be calculated as follows:
 - reserveIn_xDAI = (Mpesa_excahnge_amount - Mpesa_excahnge_amount* fee) **KSH => USD** rate (this will come out of the xDAI float account)
    
The CIC to be created (reported for quote): 

  - CIC_Created_reported = the amount of CIC to be created on chain by adding the reserveIn_xDAI (quote from blockchain)
    
After the user confirms:

  - CIC_Created = the actualy amount of CIC created on chain by adding the reserveIn_xDAI 
  - **CIC_given** = CIC_Created_reported
     -  CIC_risk =  CIC_given - CIC_Created - this should be monitored and recorded for each transaction
  
### Exchange Rate ###

When dislaying the SPOT price or excahnge rate to a user we should use the following formula:
> Exchange Price = CIC_reserve / (CIC_supply * trr) -> this is pulled from blockchain and should be sent back as a SMS


### User Interface ###



- When a user chooses to ``Redeem get Quote```:
   - They should enter the amount (limited to the max in their balance)
   - They should get a confirmation Based on a quote from the blockchain in a SMS: 
   - You will recieve **Mpesa_given** for **CIC_excahnge_amount** please confirm this quote in the USSD menue to proceed: (note this is from blockchain and will change second by second)
   - The user will return to the USSD menu
      - They navigate back to exchange in less than 1 minute and 'confirm exchange quote' (new USSD excahnge menu needed) (the 'confirm Redeem excahnge quote')
      - 'confirm Redeem quote' will only show up for a minute - otherwise they have to ask for another quote
   - Please type your PIN to confirm
   - The user should get a Mpesa responce for recieving the **Mpesa_given**

User chooses Excahnge Menu option #2
- Redeem 1000 CIC
- Calculate a quote .... : 
- Quotation: Date.
- SMS: You will receive at least 800 KSH for 1000 CIC. (MVP)
   - getQuoteRedeemCIC(amount, CIC):
      - mvp: return amount = amount - amount * rate; /// Rate is 10%
         - best: web3 calls to purchaseReturn 
- On the Exchange menu - option #3 is Confirm Quotation:
   - Please confirm: "You will receive at least 800 KSH for 1000 CIC."
   - Yes. 
      - mvp: GE receives the 1000 CIC
         - best: Then we try a conversion ... with a Min return. 
         - best: 1000 CIC is sent to the contract (on blockchain) - (xDAI is sent to the GE float account - to be cleared later)
      - Mpesa as in the quote - is sent to the user
      

- When a user chooses to ``Contribute get Quote```:
   - They should enter the amount of Mpesa they wish to contribute 
   - They should get a confirmation Based on a quote from the blockchain in a SMS: 
   - You will recieve **CIC_given** for **Mpesa_excahnge_amount** please confirm this quote in the USSD menu to proceed: (note this is from blockchain and will change second by second)
   - The user will return to the USSD menu
      - They navigate back to exchange in less than 1 minute and 'confirm exchange quote' (new USSD excahnge menu needed) (the 'confirm Contribute excahnge quote')
      - 'confirm Contribute exchange quote' will only show up for a minute - otherwise they have to ask for another quote
   - Please type your PIN to confirm
   - The user should get a SMS responce from GE for recieving the **CIC_given**

User chooses Excahnge Menu option #2
- Contribute KSH - returning CIC
- Calculate a quote .... : 
- Quotation: Date.
- SMS: You will receive at least 1200 CIC for 1000 KSH. (MVP)
   - getQuoteContriuteKSH(amount, CIC):
      - mvp: return amount = amount + amount * rate; /// Rate is 10%
         - best: web3 calls to purchaseReturn 
- On the Exchange menu - option #3 is Confirm Quotation:
   - Please confirm: "You will receive at least 1200 CIC for 1000 KSH."
   - Yes. 
      - mvp: SMS: Please send 1000 KSH to PayBill account number [phone number]
         - best: Then we try a conversion ... with a Min return. 
         - best: xDAI is sent to the contract (on blockchain) - (xDAI is sent to the GE float account - to be cleared later)
      - CIC as in the quote - is sent to the user
      





### Credit Clearing ###
Weekly or after alterts we will rebalance our xDAI and Mpesa float accounts.

As our float account gets more and more full of xDAI (and mpesa float shrinks) - we will convert the xDAI to Mpesa
via xdai.io to get ETH - then using localcryptos or BitCoinSuisse to get Kenyan Shilllings / Mpesa (note there are MANY fees along this path)

If we get a large amount of Mpesa and want to convert to xDAI -> CIC ... then we send the Mpesa eitheir to localCryptos or bitcoinSwuiss to get ETH
then we use xdai.io to convert that ETH to xDAI ... then we add that to our xDAI float


### Security ###
 - Balance alerts: On both Mpesa and CIC balances: The paybill from which Sarafu is sent needs to send a sms and email alert when low 
 - Failure alerts: If the API session fails an sms and email alert should be sent
 - Reject on not enough Mpesa balance: If there is not enough Mpesa (on incoming Sarafu) the transaction should be rejected and an email sent to admin along with a SMS to admin and user (Sarafu should not be taken from user). (message should be visible also on mgmt platform txn view and user view and admin view).
 - Reject on not enough Sarafu balance: If there is not enough Sarafu (on incoming Mpesa) the transaction should be rejected and an email sent to admin along with a SMS to admin and user (Mpesa should not be taken from user). (message should be visible also on mgmt platform txn view and user view and admin view).

