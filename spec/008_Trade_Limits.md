# Trade Limits

<!--
valid status values are: Pre-draft|Draft|Proposal|Accepted
-->
* Authors: Firstname Lastname <email> (url)
* Date: YYYY.MM.DD
* Version: 1
* Status: Pre-draft

## Rationale
<!--
Why are you proposing this?
-->
Various incentives as well as KYC requirments for us to limit trading to users until they pass certian strandards. 

## Before 
<!--
What is the current state of the described topic?
-->



## After
<!--
How will things be different after this has been done?
-->
### All User Limits
1. KYC LImits for max amount of trade per week for 
   1. phone verification = 50,000 Tokens
   1. ID verification = 100,000 Tokens
   1. secondary ID verification = 500,000 Tokens

1. Incentive Limits
   1. Max exchange to fiat 30,000 tokens
   1. Max number of excahnges per month = 1
   1. Min amount of trade before excahnge = amount being cashed out
   1. Max % of Balance = 50%


## Implementation
<!--
Here is the description of how these changes should be implemented.
Please use subheadings to improve readability.
Some suggestions:

### Workflow

### Variables

### Interface
-->

## Testing
<!--
Please describe what test vectors that are required for this implementation
-->

## Changelog
<!--
Please remember to describe every change to this document in the changelog using 
serial number:

* version 1:
-->
